package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type CampaignMonitorUnsubscribeResponse struct {
	Results []CampaignMonitorUnsubscribe
}

type CampaignMonitorUnsubscribe struct {
	ListId       string
	EmailAddress string
	Date         string
	IpAddress    string
}

func (resp *CampaignMonitorUnsubscribeResponse) ToCSV(w *csv.Writer) {
	for _, unsubscribe := range resp.Results {
		w.Write([]string{
			unsubscribe.ListId,
			unsubscribe.EmailAddress,
			unsubscribe.Date,
			unsubscribe.IpAddress,
		})
	}
}

func CampaignMonitorUnsubscribesToCsv(campaignId string) {
	var page = 1
	name := path + campaignId + "_campaign_monitor_unsubscribes.csv"
	f, err := os.Create(name)
	if err != nil {
		log.Error("Error creating file " + name)
		return
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer func() {
		w.Flush()
		if err := w.Error(); err != nil {
			log.Error("Error flushing file " + name)
		}
	}()
	for {
		resp, err := GetCampaignMonitorUnsubscribe(fmt.Sprintf("https://api.createsend.com/api/v3.2/campaigns/%s/unsubscribes.json?page=%d&pageSize=%d", campaignId, page, pageSize))
		if err != nil {
			log.Error("Error processing " + name)
			return
		}
		resp.ToCSV(w)
		if len(resp.Results) < pageSize {
			log.Debug("Finished processing " + name)
			return
		}
		page = page + 1
	}
}

func GetCampaignMonitorUnsubscribe(url string) (*CampaignMonitorUnsubscribeResponse, error) {
	resp := &CampaignMonitorUnsubscribeResponse{}
	err := GetJson(url, resp)
	return resp, err
}
