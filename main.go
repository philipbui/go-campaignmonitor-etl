package main

import (
	"os"
	"strings"
	"sync"
)

func main() {
	var campaignIds []string
	if campaignId != "" {
		campaignIds = strings.Split(campaignId, ",")
	} else {
		campaignIds = GetCampaignVouchers()
	}
	var wg = &sync.WaitGroup{}
	os.Mkdir(path, os.ModePerm)
	for _, campaignId := range campaignIds {
		if bounces {
			wg.Add(1)
			go func(campaignId string) {
				defer wg.Done()
				CampaignMonitorBouncesToCsv(campaignId)
			}(campaignId)
		}
		if opens {
			wg.Add(1)
			go func(campaignId string) {
				defer wg.Done()
				CampaignMonitorOpensToCsv(campaignId)
			}(campaignId)
		}
		if clicks {
			wg.Add(1)
			go func(campaignId string) {
				defer wg.Done()
				CampaignMonitorClicksToCsv(campaignId)
			}(campaignId)
		}
		if unsubscribes {
			wg.Add(1)
			go func(campaignId string) {
				defer wg.Done()
				CampaignMonitorUnsubscribesToCsv(campaignId)
			}(campaignId)
		}
	}
	wg.Wait()
}
