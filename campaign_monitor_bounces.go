package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type CampaignMonitorBounceResponse struct {
	Results []CampaignMonitorBounce
}

type CampaignMonitorBounce struct {
	ListId       string
	EmailAddress string
	BounceType   string
	Date         string
	Reason       string
}

func (resp *CampaignMonitorBounceResponse) ToCSV(w *csv.Writer) {
	for _, bounce := range resp.Results {
		w.Write([]string{
			bounce.ListId,
			bounce.EmailAddress,
			bounce.BounceType,
			bounce.Date,
			bounce.Reason,
		})
	}
}

func CampaignMonitorBouncesToCsv(campaignId string) {
	var page = 1
	name := path + campaignId + "_campaign_monitor_bounces.csv"
	f, err := os.Create(name)
	if err != nil {
		log.Error("Error creating file " + name)
		return
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer func() {
		w.Flush()
		if err := w.Error(); err != nil {
			log.Error("Error flushing file " + name)
		}
	}()
	for {
		resp, err := GetCampaignMonitorBounce(fmt.Sprintf("https://api.createsend.com/api/v3.2/campaigns/%s/bounces.json?page=%d&pageSize=%d", campaignId, page, pageSize))
		if err != nil {
			log.Error("Error processing " + name)
			return
		}
		resp.ToCSV(w)
		if len(resp.Results) < pageSize {
			log.Debug("Finished processing " + name)
			return
		}
		page = page + 1
	}
}

func GetCampaignMonitorBounce(url string) (*CampaignMonitorBounceResponse, error) {
	resp := &CampaignMonitorBounceResponse{}
	err := GetJson(url, resp)
	return resp, err
}
