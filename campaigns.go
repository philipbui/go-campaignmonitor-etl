package main

import (
	"fmt"
	"strings"
)

type CampaignResponse = []Campaign

type Campaign struct {
	Name       string
	CampaignID string
}

func GetCampaignVouchers() []string {
	resp, err := GetCampaign(fmt.Sprintf("https://api.createsend.com/api/v3.2/clients/%s/campaigns.json", clientKey))
	if err != nil {
		log.Error("Error getting Campaign Vouchers")
	}
	campaignIds := make([]string, len(resp))
	for _, campaignId := range resp {
		if strings.HasSuffix(campaignId.Name, "_withinsix") || strings.HasSuffix(campaignId.Name, "_notwithinsix") {
			campaignIds = append(campaignIds, campaignId.CampaignID)
		}
	}
	return campaignIds
}

func GetCampaign(url string) (CampaignResponse, error) {
	resp := CampaignResponse{}
	err := GetJson(url, &resp)
	return resp, err
}
