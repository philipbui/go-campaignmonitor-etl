package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type CampaignMonitorOpenResponse struct {
	Results []CampaignMonitorOpen
}

type CampaignMonitorOpen struct {
	ListId       string
	EmailAddress string
	Date         string
	CountryCode  string
	CountryName  string
	Region       string
	City         string
	Latitude     string
	Longitude    string
	IpAddress    string
}

func (resp *CampaignMonitorOpenResponse) ToCSV(w *csv.Writer) {
	for _, open := range resp.Results {
		w.Write([]string{
			open.ListId,
			open.EmailAddress,
			open.Date,
			open.CountryCode,
			open.CountryName,
			open.Region,
			open.City,
			open.Latitude,
			open.Longitude,
			open.IpAddress,
		})
	}
}

func CampaignMonitorOpensToCsv(campaignId string) {
	var page = 1
	name := path + campaignId + "_campaign_monitor_opens.csv"
	f, err := os.Create(name)
	if err != nil {
		log.Error("Error creating file " + name)
		return
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer func() {
		w.Flush()
		if err := w.Error(); err != nil {
			log.Error("Error flushing file " + name)
		}
	}()
	for {
		resp, err := GetCampaignMonitorOpen(fmt.Sprintf("https://api.createsend.com/api/v3.2/campaigns/%s/opens.json?page=%d&pageSize=%d", campaignId, page, pageSize))
		if err != nil {
			log.Error("Error processing " + name)
			return
		}
		resp.ToCSV(w)
		if len(resp.Results) < pageSize {
			log.Debug("Finished processing " + name)
			return
		}
		page = page + 1
	}
}

func GetCampaignMonitorOpen(url string) (*CampaignMonitorOpenResponse, error) {
	resp := &CampaignMonitorOpenResponse{}
	err := GetJson(url, resp)
	return resp, err
}
