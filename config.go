package main

import "flag"

var (
	campaignId   string
	clientKey    string
	apiKey       string
	bounces      bool
	path         string
	opens        bool
	clicks       bool
	unsubscribes bool
	pageSize     int
)

func init() {
	flag.StringVar(&campaignId, "campaignId", "", "")
	flag.StringVar(&clientKey, "clientKey", "", "")
	flag.StringVar(&apiKey, "apiKey", "", "")
	flag.BoolVar(&bounces, "bounces", true, "")
	flag.StringVar(&path, "path", "output", "")
	path = path + "/"
	flag.BoolVar(&opens, "opens", true, "")
	flag.BoolVar(&clicks, "clicks", true, "")
	flag.BoolVar(&unsubscribes, "unsubscribes", true, "")
	flag.IntVar(&pageSize, "size", 1000, "")
	flag.Parse()
}
