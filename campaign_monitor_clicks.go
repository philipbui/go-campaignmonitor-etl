package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type CampaignMonitorClickResponse struct {
	Results []CampaignMonitorClick
}

type CampaignMonitorClick struct {
	ListId       string
	EmailAddress string
	Url          string
	Date         string
	CountryCode  string
	CountryName  string
	Region       string
	City         string
	Latitude     string
	Longitude    string
	IpAddress    string
}

func (resp *CampaignMonitorClickResponse) ToCSV(w *csv.Writer) {
	for _, click := range resp.Results {
		w.Write([]string{
			click.ListId,
			click.EmailAddress,
			click.Url,
			click.Date,
			click.CountryCode,
			click.CountryName,
			click.Region,
			click.City,
			click.Latitude,
			click.Longitude,
			click.IpAddress,
		})
	}
}

func CampaignMonitorClicksToCsv(campaignId string) {
	var page = 1
	name := path + campaignId + "_campaign_monitor_clicks.csv"
	f, err := os.Create(name)
	if err != nil {
		log.Error("Error creating file " + name)
		return
	}
	defer f.Close()
	w := csv.NewWriter(f)
	defer func() {
		w.Flush()
		if err := w.Error(); err != nil {
			log.Error("Error flushing file " + name)
		}
	}()
	for {
		resp, err := GetCampaignMonitorClick(fmt.Sprintf("https://api.createsend.com/api/v3.2/campaigns/%s/clicks.json?page=%d&pageSize=%d", campaignId, page, pageSize))
		if err != nil {
			log.Error("Error processing " + name)
			return
		}
		resp.ToCSV(w)
		if len(resp.Results) < pageSize {
			log.Debug("Finished processing " + name)
			return
		}
		page = page + 1
	}
}

func GetCampaignMonitorClick(url string) (*CampaignMonitorClickResponse, error) {
	resp := &CampaignMonitorClickResponse{}
	err := GetJson(url, resp)
	return resp, err
}
