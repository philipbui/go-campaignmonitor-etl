package main

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"time"
)

var client *http.Client

func init() {
	client = &http.Client{
		Timeout:       10 * time.Second,
		CheckRedirect: checkRedirect,
	}
}
func campaignMonitorAuthorization() string {
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(apiKey+":"))
}

func checkRedirect(req *http.Request, via []*http.Request) error {
	req.Header.Add("Authorization", campaignMonitorAuthorization())
	return nil
}

func GetJson(url string, target interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Add("Authorization", campaignMonitorAuthorization())
	resp, err := client.Do(req)
	if err != nil {
		log.Error("/GET " + url + ": " + err.Error())
		return err
	}
	defer resp.Body.Close()
	log.Debug("/GET " + url)
	json.NewDecoder(resp.Body).Decode(target)
	return nil
}
